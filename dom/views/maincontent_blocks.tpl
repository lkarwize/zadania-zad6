%def leftmenu():
    %if logged_in == 0:
        <ul>
            <li><a href="/">Wpisy</a></li>
            <li><a href="/login">Logowanie</a></li>
            <li><a href="/register">Rejestracja</a></li>
            <li><a href="/kontakt">Kontakt</a></li>
        </ul>
    %end
    %if logged_in != 0:
        <ul>
            <li><a href="/">Wpisy</a></li>
            <li><a href="/logout">Wyloguj</a></li>
            <li><a href="/addbottle">Dodaj wpis</a></li>
            <li><a href="/kontakt">Kontakt</a></li>
        </ul>
     %end
%end
%def maincontent():
    <div class="content">
        %include
    </div>
%end

%rebase maincontent leftmenu=leftmenu, maincontent=maincontent, title=title