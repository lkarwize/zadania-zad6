<html>
<head>
    <title>{{title or "BOOTLEblog"}}</title>
    <link rel="stylesheet" href="/css/main.css" />
</head>
<body>

    <div class="maincontent">
        <div class="top">
            <h1>{{title or "BOOTLEblog"}}</h1>
        </div>
        <div class="content">
            %include
        </div>
    </div>
</body>
</html>