<div class="{{message_class}}">
    <div class="{{message_class}}-content">
        {{message}}
    </div>
</div>
%rebase maincontent_blocks title = '', logged_in = logged_in