%rebase maincontent_blocks title=title, logged_in=logged_in
%if not articles:
    <div class="no-articles">Brak artykułów</div>
%else:
%for article in articles:
    <div class="article-item">
        <div class="article-content">{{article[1]}}</div>
        <div class=article-footer>
            <span class="article-author">{{article[0]}}</span>
            <span class="article-date">{{article[2]}}</span>
        </div>
    </div>
%end