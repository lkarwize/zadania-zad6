drop table if exists users;
create table users (
    id integer primary key autoincrement,
    login text not null,
    password text not null,
    email string not null
);

/*pierwszy testowy user */
INSERT INTO users (login, password, email) VALUES ('admin', 'pass123', 'mjanisiewicz@gmail.com');

drop table if exists bottles;
create table bottles (
    id integer primary key autoincrement,
    login string not null,
    bottle string not null,
    added date not null DEFAULT CURRENT_TIMESTAMP
);
/*pierwszy testowy rekord*/
INSERT INTO bottles (login, bottle) VALUES ('admin', 'To moja pierwsza butelka wysłana w świat');