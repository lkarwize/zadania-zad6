# -*- coding: utf-8 -*-
import bottle
from bottle import request, route, template, static_file, post, error, install, redirect
from bottle_sqlite import SQLitePlugin
from beaker.middleware import SessionMiddleware
#obsluga sesji dzieki bibliotece Beaker
session_opts = {
    'session.type': 'file',
    'session.cookie_expires': 300,
    'session.data_dir': './data',
    'session.auto': True
}
app = SessionMiddleware(bottle.app(), session_opts)
install(SQLitePlugin(dbfile='/home/p12/dom/db/db.db')) # SQLite plugin do Bottle
bottle.TEMPLATE_PATH.insert(0,'/home/p12/dom/views')
maintitle = "BOTTLEblog"
@route('/')  # glowna
def index(db):
    logged = logged_in()
    articles = get_bottles(db)
    return template('articles', title=maintitle, articles=articles, logged_in=logged)

@route('/kontakt')  # kontakt
def kontakt():
    logged = logged_in()
    return template('contact', title=maintitle + " / Kontakt", logged_in = logged)

@route('/login')  # login
def login():
    logged = logged_in()
    return template('loginform', title=maintitle + " / Login", logged_in=logged)

@post('/login')  # obsluga post loginu
def login_post(db):
    logged = logged_in()
    if not logged:
        name = request.forms.get('login')
        pwd = request.forms.get('pass')
        row = check_login(db, name, pwd)
        if row:
            # obsluga sesji jesli zalogowano
            s = bottle.request.environ.get('beaker.session')
            s['logged_in'] = name
            s.save()
            redirect('/')
        else:
            return template('error', message='Nie zalogowano', message_class='error', logged_in=logged)
    else:
        redirect('/')

@route('/logout')
def logout():
    logged = logged_in()
    if logged:
        s = bottle.request.environ.get('beaker.session')
        s['logged_in'] = 0
        s.save()
    redirect('/')

@route('/register')  # rejestracja
def register():
    logged = logged_in()
    if not logged:
        return template('registerform', title=maintitle + " / Rejestracja", logged_in=logged)
    else:
        redirect('/')
@post('/register')  # obsluga post rejestracji
def register_post(db):
    logged = logged_in()
    if not logged:
        name = request.forms.get('login')
        pwd = request.forms.get('pass')
        email = request.forms.get('email')
        exists = check_if_users_exists(db, name)
        if exists:
            return template('error', message='Użytkownik już istnieje', message_class='error', logged_in=logged)  # tutaj raczej pokazac formularz
        else:
            try:
                db.execute("INSERT INTO users (login, password, email) VALUES (?, ?, ?)", [name, pwd, email])
            except:
                return template('error', message='Upewnij się, że wpisałeś wszystkie dane', message_class='error', logged_in=logged)
            return template('error', message='Rejestracja przebiegła pomyślnie', message_class='success', logged_in=logged)
    else:
        redirect('/')

@route('/addbottle')  # dodawanie wpisu
def add():
    logged = logged_in()
    if logged:
        return template('addform', title=maintitle + '/ Dodaj wpis', logged_in=logged)
    else:
        return template('error', message='Musisz być zalogowany aby dodać wpis', message_class='error')

@post('/addbottle')  # post dodawania wpisu
def add_post(db):
    logged = logged_in()
    if logged:
        content = request.forms.get('content')
        if len(content) > 0:
            try:
                db.execute("INSERT INTO bottles (login, bottle) VALUES(?, ?)", [logged, content] )
                return template('error', message='Dodano pomyślnie', message_class='success', logged_in=logged)
            except:
                return template('error', message='Upewnij się, że wpisałeś wszystkie dane', message_class='error', logged_in=logged)
        else:
            return template('error', message='Musisz podać treść', message_class='error', logged_in=logged)
    else:
        redirect('/')

#statyczne pliki, obsluga errorow
@route('/css/<filename>')
def server_static(filename):
    return static_file(filename, root='/home/p12/dom/files/css/')

@route('/js/<filename>')
def server_static(filename):
    return static_file(filename, root='/home/p12/dom/files/js/')

@error(404)
def error404(error):
    return 'Nie ma takiej stronyy'

@error(405)
def error405(error):
    return 'Niedozwolone działanie'

#@error(500)
#def error500(error):
#    return 'Błąd, spróbuj później'

#proste funkcje pomocnicze
def check_if_users_exists(db, name):
    row = db.execute("SELECT * FROM users WHERE login=?", [name]).fetchone()
    if row:
        return True
    else:
        return False

def check_login(db, name, pwd):
    row = db.execute('SELECT * FROM users WHERE login=? AND password=?', [name, pwd]).fetchone()
    if row:
        return True
    else:
        return False

def logged_in():
    s = bottle.request.environ.get('beaker.session')
    s_ = s.get('logged_in', 0)
    if not s_ == 0:
        s['logged_in'] = s_ #przedluzamy sesje przy jej odczycie
        s.save()
        return s_
    else:
        return False

def get_bottles(db):
    query = db.execute("SELECT login, bottle, added FROM bottles").fetchall()
    return query

#bottle.run(app=app, host='', port=31012, debug=True, reloader=True)